# UiPath-TestingFrameworkUi #

Is a GUI made to be used with [UiPath-Testing_Framework](https://bitbucket.org/krsma33/uipath-testing_framework/src/master/).

When used with the UiPath Testing Framework, GUI serves multiple purposes:

* Selecting which tests to run - GUI reads all the tests found in **Tests_Repository** folder and its subfolders.
* Showing the test results
* Allowing multiple test runs during single _RunAllTests.xaml_ execution. Via use of the InOut arguments to perserve information about runned  tests, window information, etc...

GUI quick guide:

* Select a specific test or a folder from the GUI tree view (use CTRL + left click to select multiple items).
* Run selected test(s) using one of the options:
	* Press Run All text at the top of the GUI.
	* Press Run Selected text at the top of the GUI (this can also be performed by right clicking on a tree view item and selecting appropriate option from the context menu).
	* Press Run Templates... text at the top of the GUI and choose appropriate option.
* After run is complete navigate the GUI to get the information about the ran tests.
* If needed perform additional test runs.
* When finished just close the GUI by pressing the "x" button.

NOTE: To edit test workflows while the Testing Framework is running, test needs to be opened in a second UiPath instance. This can quickly be done by right clicking the test and selecting **Edit test** option from the context menu.

## Get Test Locations Ui Activity ##

To display the GUI in the UiPath after installation of the package search for the **Get Test Locations Ui** activity.

Activity properties:

* Input arguments:
	* (string) RootTestFolder - Initial folder path from which .xaml files will be searched from
	* (string) ProjectName - Name of the project that will be displayed in the GUI
* Output arguments:
	* (Dictionary<string, List<string>>) TestsToRun - Dictionary that contains list of tests to be run. Keys represent the test folder names. Values represent list of test paths that belong to specified folder.
* InOut arguments:
	* (SortedDictionary<string, Dictionary<string, object>>) TestRunInformation - Information about already runned tests. This is used so that information is persisted for the re-runs. On the first test run it should be null. If not empty following keys will be present in the dictionary: TestPassed of type bool, ErrorMessage of type string, NewResult of type bool, and ElapsedTime of type string.
	* (Dictionary<string, bool>) ExpandedItems - Information about expanded items in previous test run. This is used so that information is persisted for the re-runs. On the first test run it should be null.
	* (Dictionary<string, object>) WindowInformation - Information about UI position, size and maximized state. This is used so that information is persisted for the re-runs. On the first test run it should be null.