﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WPFUI
{
    [ValueConversion(typeof(double), typeof(GridLength))]
    public class WindowWidthToGridColumnWidth : IValueConverter
    {
        public static WindowWidthToGridColumnWidth Instance = new WindowWidthToGridColumnWidth();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var width = (double)value;

            if (width > 900)
            {
                return new GridLength(1, GridUnitType.Star);
            }
            else
            {
                return new GridLength(1, GridUnitType.Auto);
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}