﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using WPFUI.Views;

namespace WPFUI
{
    /// <summary>
    /// Convert a TestResults to the Foreground Colour
    /// </summary>
    [ValueConversion(typeof(TestResults), typeof(Brush))]
    public class TreeItemTextColourConverter : IValueConverter
    {
        public static TreeItemTextColourConverter Instance = new TreeItemTextColourConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var testResults = (TestResults)value;

            Brush brush;

            if (DirectoryStructureView.TestResults == null)
            {
                brush = Brushes.White;
            }
            else
            {
                if (testResults.TestPassed == null)
                {
                    brush = Brushes.Gray;
                }
                else
                {
                    if (testResults.NewResult == true)
                    {
                        brush = Brushes.White;
                    }
                    else
                    {
                        brush = Brushes.Gray;
                    }
                }
            }

            return brush;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
