﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using WPFUI.Views;

namespace WPFUI
{
    /// <summary>
    /// Convert a TestResults to the specific image type
    /// </summary>
    [ValueConversion(typeof(TestResults), typeof(BitmapImage))]
    public class HeaderToImageConverter : IValueConverter
    {
        public static HeaderToImageConverter Instance = new HeaderToImageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var testResults = (TestResults)value;

            // By default, we presume an image
            var image = "../Icons/";

            if (DirectoryStructureView.TestResults == null)
            {
                image += "info.png";
            }
            else
            {
                if (testResults.TestPassed == null)
                {
                    image += "info_grayed.png";
                }
                else
                {
                    if (testResults.NewResult == true)
                    {
                        if (testResults.TestPassed == true)
                        {
                            image += "accept.png";
                        }
                        else
                        {
                            image += "deny.png";
                        }
                    }
                    else
                    {
                        if (testResults.TestPassed == true)
                        {
                            image += "accept_grayed.png";
                        }
                        else
                        {
                            image += "deny_grayed.png";
                        }
                    }
                }
            }

            //return new BitmapImage(new Uri($"pack://application:,,,/{ image }"));
            //return new BitmapImage(new Uri(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, image)));
            return new BitmapImage(new Uri($"{ image }", UriKind.Relative));

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
