﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace WPFUI
{
    /// <summary>
    /// Convert a Test Status to the specific image type
    /// </summary>
    [ValueConversion(typeof(string), typeof(BitmapImage))]
    public class TestStatusToImageConverter : IValueConverter
    {
        public static TestStatusToImageConverter Instance = new TestStatusToImageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            var testResults = (string)value;

            // By default, we presume an image
            var image = "../Icons/";

            if (testResults == "Test Passed")
            {
                image += "accept.png";
            }
            else if (testResults == "Test Failed")
            {
                image += "deny.png";
            }
            else if (testResults != null)
            {
                if (testResults.Contains("Passed Tests"))
                {
                    image += "accept.png";
                }
                else if (testResults.Contains("Failed Tests"))
                {
                    image += "deny.png";
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

            //return new BitmapImage(new Uri($"pack://application:,,,/{ image }"));
            //return new BitmapImage(new Uri(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, image)));
            return new BitmapImage(new Uri($"{ image }", UriKind.Relative));

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
