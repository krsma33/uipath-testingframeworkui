﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFUI
{
    [ValueConversion(typeof(double), typeof(double))]
    public class WindowWidthToGridRowConverter : IValueConverter
    {
        public static WindowWidthToGridRowConverter Instance = new WindowWidthToGridRowConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var width = (double)value;

            if (width > 900)
            {
                return 2;
            }
            else
            {
                return 3;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
