﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace WPFUI
{
    /// <summary>
    /// Convert a Type to the specific margin thickness
    /// </summary>
    [ValueConversion(typeof(TestResults), typeof(Thickness))]
    public class ItemTypeToImageMarginConverter : IValueConverter
    {
        public static ItemTypeToImageMarginConverter Instance = new ItemTypeToImageMarginConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var testResults = (TestResults)value;

            //Thickness thickness;

            //// Moves files a bit to the right if in their folder at least one sub-folder exists
            //if (testResults.Type == DirectoryItemType.File)
            //{
            //    var directoryPath = Path.GetDirectoryName(testResults.FilePath);

            //    var directories = Directory.GetFiles(directoryPath, "*.xaml", SearchOption.AllDirectories).Where(x => x.Replace('/','\\').Substring(directoryPath.Length+1).Count(y => y.Equals('\\')) < 2).Select(x => Path.GetDirectoryName(x)).Where(x => x.Equals(directoryPath) == false);

            //    bool filesAndFolders = directories.Count() > 0;

            //    thickness = filesAndFolders ? new Thickness(15, 5, 5, 5) : new Thickness(5);
            //}
            //else
            //{
            //    thickness = new Thickness(5);
            //}

            //return thickness;

            return new Thickness(5);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
