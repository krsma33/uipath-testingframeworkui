﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using WPFUI.ViewModels;

namespace WPFUI.Views
{
    /// <summary>
    /// Interaction logic for DirectoryStructureView.xaml
    /// </summary>
    public partial class DirectoryStructureView : Window
    {

        #region Static Properties

        /// <summary>
        /// Holds Name of the Project
        /// </summary>
        public static string ProjectName = "Testing Framework";

        /// <summary>
        /// Holds initial path to the folder that will be used as root
        /// </summary>
        public static string RootFolderPath = @"D:\Documents for Projects\03_Uipath\UiPath_TestingFramework\Tests_Repository";

        /// <summary>
        /// Test results. Keys represent File Paths. Values are Dictionaries of string, object with following keys: TestPassed of type bool, ErrorMessage of type string, NewResult of type bool, and ElapsedTime of type string.
        /// </summary>
        public static SortedDictionary<string, Dictionary<string, object>> TestResults { get; set; }

        /// <summary>
        /// Keeps information about the expanded items in the tree view.
        /// </summary>
        public static Dictionary<string, bool> ExpandedItems { get; set; }

        /// <summary>
        /// Keeps information about the window position, size and maximized status.
        /// </summary>
        public static Dictionary<string, object> WindowInformation { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public DirectoryStructureView()
        {
            InitializeComponent();
            
            MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight-2;

            SetWindowPropertiesOnInitialization();

            if (WindowInformation == null)
                GetWindowProperties();

            SizeChanged += DirectoryStructureView_SizeChanged;
            LocationChanged += DirectoryStructureView_LocationChanged;

            var viewModel = new DirectoryStructureViewModel(this.Close);

            DataContext = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Fill WindowInformation dictionary on position change
        /// </summary>
        private void DirectoryStructureView_LocationChanged(object sender, EventArgs e)
        {
            GetWindowProperties();
        }

        /// <summary>
        /// Fill WindowInformation dictionary on size change
        /// </summary>
        private void DirectoryStructureView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GetWindowProperties();
        }

        /// <summary>
        /// TitleBar_MouseDown - Drag if single-click, resize if double-click
        /// </summary>
        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                if (e.ClickCount == 2)
                {
                    AdjustWindowSize();
                }
                else
                {
                    DragMove();
                }
        }

        /// <summary>
        /// MaximizedButton_Clicked
        /// </summary>
        private void MaximizeButton_Click(object sender, RoutedEventArgs e)
        {
            AdjustWindowSize();
        }

        /// <summary>
        /// Minimized Button_Clicked
        /// </summary>
        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Opens Context Menu on left click
        /// </summary>
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RunTemplatesLink.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            RunTemplatesLink.ContextMenu.PlacementTarget = (UIElement)sender;
            RunTemplatesLink.ContextMenu.IsOpen = true;
        }

        /// <summary>
        /// Makes sure that window will be maximized on the proper screen
        /// </summary>
        private void DirectoryStructureView_SourceInitialized(object sender, EventArgs e)
        {
            WindowState = !_intitialMaximizedState ? WindowState.Maximized : WindowState.Normal;
            AdjustWindowSize();
        }

        /// <summary>
        /// Selects tree view item when right click is performed
        /// </summary>
        private void MultiSelectTreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);

            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles not bringing into view on TreeViewItem click if it has horizontal scrollbar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Iterates over dependency objects to find the source parent
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);

            return source as TreeViewItem;
        }

        /// <summary>
        /// Gets window properties and add them to the WindowInformation dictionary
        /// </summary>
        private void GetWindowProperties()
        {
            if (WindowInformation == null)
                WindowInformation = new Dictionary<string, object>();

            WindowInformation["IsMaximized"] = WindowState == WindowState.Maximized ? true : false;

            if (WindowState != WindowState.Maximized)
            {
                WindowInformation["Height"] = ActualHeight;
                WindowInformation["Width"] = ActualWidth;
                WindowInformation["Top"] = Top;
                WindowInformation["Left"] = Left;
            }
        }

        private bool _intitialMaximizedState = false;

        /// <summary>
        /// Setup the window position and dimensions based on persisted data
        /// </summary>
        private void SetWindowPropertiesOnInitialization()
        {
            if (WindowInformation != null)
            {
                if (!WindowInformation.TryGetValue("Height", out object height)) return;
                if (!WindowInformation.TryGetValue("Width", out object width)) return;
                if (!WindowInformation.TryGetValue("Top", out object top))  return;
                if (!WindowInformation.TryGetValue("Left", out object left)) return;
                if (!WindowInformation.TryGetValue("IsMaximized", out object isMaximized)) return;

                _intitialMaximizedState = (bool)isMaximized;

                WindowStartupLocation = WindowStartupLocation.Manual;

                Height = (double)height;
                Width = (double)width;
                Top = (double)top;
                Left = (double)left;

                SourceInitialized += DirectoryStructureView_SourceInitialized;

            }
        }

        /// <summary>
        /// Adjusts the WindowSize to correct parameters when Maximize button is clicked
        /// </summary>
        private void AdjustWindowSize()
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                MaximizeButton.Content = "1";
                MainBorder.BorderThickness = new Thickness(0);
            }
            else
            {
                WindowState = WindowState.Maximized;
                MaximizeButton.Content = "2";
                MainBorder.BorderThickness = new Thickness(8);
            }
        }

        #endregion


    }
}
