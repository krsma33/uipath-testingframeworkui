﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using WPFUI.Views;

namespace WPFUI
{
    /// <summary>
    /// Helper class to query information about directories
    /// </summary>
    public static class DirectoryStructure
    {

        /// <summary>
        /// Returns TestResults object based on the input data.
        /// </summary>
        /// <param name="fullPath">Full path to the directory item</param>
        /// <param name="type">Type of the directory item</param>
        /// <returns></returns>
        public static TestResults SetTestResults(string fullPath, DirectoryItemType type)
        {
            var testResultsDictionary = DirectoryStructureView.TestResults;

            if (testResultsDictionary == null)
            {
                return new TestResults(fullPath, null, null, null, type, new TimeSpan());
            }

            if (type == DirectoryItemType.File)
            {
                if (testResultsDictionary.TryGetValue(fullPath, out Dictionary<string, object> testInfo))
                {
                    testInfo.TryGetValue("TestPassed", out object testPassed);
                    testInfo.TryGetValue("ErrorMessage", out object errorMessage);
                    testInfo.TryGetValue("NewResult", out object newResult);
                    testInfo.TryGetValue("ElapsedTime", out object elapsedTime);

                    return new TestResults(fullPath, (bool)testPassed, errorMessage as string, (bool)newResult, type, (TimeSpan)elapsedTime);
                }
                else
                {
                    return new TestResults(fullPath, null, null, null, type, new TimeSpan());
                }
            }
            else
            {
                var filteredDictionary = testResultsDictionary.Where(x => x.Key.Replace('/', '\\').Contains(fullPath.Replace('/', '\\')+"\\"));

                if (filteredDictionary.Count() > 0)
                {
                    bool newResult = filteredDictionary.Any(x => (bool)x.Value["NewResult"] == true);

                    bool testPassed = newResult ? filteredDictionary.Where(x => (bool)x.Value["NewResult"] == true).All(x => (bool)x.Value["TestPassed"] == true) : filteredDictionary.All(x => (bool)x.Value["TestPassed"] == true);

                    return new TestResults(fullPath, testPassed, null, newResult, type, new TimeSpan());
                }
                else
                {
                    return new TestResults(fullPath, null, null, null, type, new TimeSpan());
                }
            }
        }

        /// <summary>
        /// Get all logical drives on the machine
        /// </summary>
        /// <returns></returns>
        public static List<DirectoryItem> GetLogicalDrives()
        {
            // Get every logical drive on the machine
            return Directory.GetLogicalDrives().Select(drive => new DirectoryItem { FullPath = drive, Type = DirectoryItemType.Drive }).ToList();
        }

        /// <summary>
        /// Gets list of directory items. This list only contains one item, which represents the root folder based on fullPath argument.
        /// </summary>
        /// <returns></returns>
        public static List<DirectoryItem> GetRootLocation(string fullPath)
        {
            // Get every logical drive on the machine
            var directories = new List<DirectoryItem>
            {
                new DirectoryItem { FullPath = fullPath, Type = DirectoryItemType.Folder }
            };
            return directories;
        }

        /// <summary>
        /// Gets the directories top-level content
        /// </summary>
        /// <param name="fullPath">The full path to the directory</param>
        /// <param name="eligibleFilePaths">File paths array from which found folders can be chosen - Used for filtering</param>
        /// <returns></returns>
        public static List<DirectoryItem> GetDirectoryContents(string fullPath, string[] eligibleFilePaths)
        {
            // Create empty list
            var items = new List<DirectoryItem>();

            #region Get Folders

            // Try and get directories from the folder
            // ignoring any issues doing so
            try
            {
                var dirs = Directory.GetDirectories(fullPath).Where(x => eligibleFilePaths.Any(y => y.Replace('/', '\\').Contains(x.Replace('/', '\\') + "\\"))).ToArray();

                if (dirs.Length > 0)
                    items.AddRange(dirs.Select(dir => new DirectoryItem { FullPath = dir, Type = DirectoryItemType.Folder}));
            }
            catch { }

            #endregion

            #region Get Files

            // Try and get .xaml files from the folder
            // ignoring any issues doing so
            try
            {
                var fs = Directory.GetFiles(fullPath).Where(x => x.Contains(".xaml"));

                if (fs.Count() > 0)
                    items.AddRange(fs.Select(file => new DirectoryItem { FullPath = file, Type = DirectoryItemType.File }));
            }
            catch { }

            #endregion

            return items;

        }

        /// <summary>
        /// Gets the directories top-level content asynchronously
        /// </summary>
        /// <param name="fullPath">The full path to the directory</param>
        /// <param name="eligibleFilePaths">File paths array from which found folders can be chosen - Used for filtering</param>
        /// <returns></returns>
        public static async Task<List<DirectoryItem>> GetDirectoryContentsAsync(string fullPath, string[] eligibleFilePaths)
        {
            // Create empty list
            var items = new List<DirectoryItem>();

            #region Get Folders

            // Try and get directories from the folder
            // ignoring any issues doing so
            try
            {
                var dirs = await Task.Run(() => Directory.GetDirectories(fullPath).Where(x => eligibleFilePaths.Any(y => y.Replace('/', '\\').Contains(x.Replace('/', '\\') + "\\"))).ToArray());

                if (dirs.Length > 0)
                    await Task.Run(() => items.AddRange(dirs.Select(dir => new DirectoryItem { FullPath = dir, Type = DirectoryItemType.Folder })));
            }
            catch { }

            #endregion

            #region Get Files

            // Try and get .xaml files from the folder
            // ignoring any issues doing so
            try
            {
                var fs = await Task.Run(() => Directory.GetFiles(fullPath).Where(x => x.Contains(".xaml")));

                if (fs.Count() > 0)
                    await Task.Run(() => items.AddRange(fs.Select(file => new DirectoryItem { FullPath = file, Type = DirectoryItemType.File })));
            }
            catch { }

            #endregion

            return items;

        }

        #region Helpers

        /// <summary>
        /// Find the file or folder name from a full path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileFolderName(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            // Make all slashes backslashes
            var normalizedPath = path.Replace('/', '\\');

            // Find the last backslash in the path
            var lastIndex = normalizedPath.LastIndexOf('\\');

            // If we don't find a backslash, return the path itself
            if (lastIndex <= 0)
                return path;

            // Return the name after the last backslash
            return path.Substring(lastIndex + 1);
        }

        /// <summary>
        /// Gets total number of .xaml files that are in all of the subfolders of a specified path
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string GetXamlFilesCount(string fullPath)
        {
            if (String.IsNullOrEmpty(fullPath))
                return string.Empty;

            if (Path.HasExtension(fullPath))
                return string.Empty;

            return Directory.EnumerateFiles(fullPath, "*.xaml", SearchOption.AllDirectories).Count().ToString();
        }

        /// <summary>
        /// Gets all the .xaml files that are in all of the subfolders of a specified path
        /// </summary>
        /// <param name="parentPath"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetXamlFilesPaths(string parentPath)
        {
            var filePaths = new List<string>();

            if (String.IsNullOrEmpty(parentPath))
                return filePaths;

            if (Path.HasExtension(parentPath))
            {
                filePaths.Add(parentPath);
                return filePaths;
            }

            return Directory.EnumerateFiles(parentPath, "*.xaml", SearchOption.AllDirectories);
        }

        #endregion
    }
}
