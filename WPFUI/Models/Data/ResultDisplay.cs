﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFUI
{
    public class ResultDisplay
    {
        /// <summary>
        /// Title to display in selected test(s) window.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Full path to the test or the tests folder.
        /// </summary>
        public string FullPath { get; }

        /// <summary>
        /// Represents status of the test. True = passed, False = failed, null = not ran.
        /// </summary>
        private readonly bool? TestPassed;


        /// <summary>
        /// Reason why test failed.
        /// </summary>
        private readonly string ErrorMessage;

        /// <summary>
        /// Test run duration in string format. 
        /// </summary>
        private readonly string ElapsedTime;

        /// <summary>
        /// Total test run duration if multiple tests are selected.
        /// </summary>
        private readonly string TotalElapsedTime;

        /// <summary>
        /// Total number of passed tests if multiple tests are selected.
        /// </summary>
        public string TotalPassed { get; }

        /// <summary>
        /// Total number of failed tests if multiple tests are selected.
        /// </summary>
        public string TotalFailed { get; }

        /// <summary>
        /// Title next to the source path of the test or the tests folder.
        /// </summary>
        public string SourceTitle
        {
            get
            {
                if (File.Exists(FullPath))
                {
                    return "Test:";
                }
                else if (Directory.Exists(FullPath))
                {
                    return "Test folder:";
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// String representation of the test status
        /// </summary>
        public string TestStatus
        {
            get
            {
                if (TestPassed == null)
                {
                    return null;
                }
                else if (TestPassed == true)
                {
                    return "Test Passed";
                }
                else
                {
                    return "Test Failed";
                }
            }
        }

        /// <summary>
        /// Full representation of the error message
        /// </summary>
        public string Message
        {
            get
            {
                if (TotalElapsedTime != null)
                {
                    return null;
                }
                else
                {
                    var result = TestPassed == null ? null : TestPassed == true ? null : ErrorMessage.ToString();
                    return result;
                }
            }
        }

        /// <summary>
        /// Label for ErrorMessage
        /// </summary>
        public string MessageLabel { get; set; } = "Message:";

        /// <summary>
        /// Full representation of the test run time.
        /// </summary>
        public string TestRunTime
        {
            get
            {
                if (ElapsedTime != null)
                {
                    return ElapsedTime.ToString();
                }
                else
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Label for TestRunTime
        /// </summary>
        public string TestRunTimeLabel { get; set; } = "Elapsed time:";

        /// <summary>
        /// Full representation of the total test run time in case of multiple selection.
        /// </summary>
        public string TotalTestRunTime
        {
            get
            {
                if (TotalElapsedTime != null)
                {
                    return TotalElapsedTime.ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Label for TotalTestRunTime
        /// </summary>
        public string TotalTestRunTimeLabel { get; set; } = "Total run time:";

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="fullPath"></param>
        /// <param name="testPassed"></param>
        /// <param name="errorMessage"></param>
        /// <param name="elapsedTime"></param>
        /// <param name="totalElapsedTime"></param>
        /// <param name="totalPassed"></param>
        /// <param name="totalFailed"></param>
        public ResultDisplay(string title, string fullPath, bool? testPassed, string errorMessage, string elapsedTime, string totalElapsedTime, string totalPassed, string totalFailed)
        {
            Title = title;
            FullPath = fullPath;
            TestPassed = testPassed;
            ErrorMessage = errorMessage;
            ElapsedTime = elapsedTime;
            TotalElapsedTime = totalElapsedTime;
            TotalPassed = totalPassed;
            TotalFailed = totalFailed;
        }
    }
}
