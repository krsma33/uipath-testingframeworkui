﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFUI
{
    /// <summary>
    /// Results of the .xaml tests ran by the UiPath Testing Framework
    /// </summary>
    public class TestResults
    {
        /// <summary>
        /// Full path to the test file or tests folder.
        /// </summary>
        public string FilePath { get; }

        /// <summary>
        /// Represents status of the test. True = passed, False = failed, null = not ran.
        /// </summary>
        public bool? TestPassed { get; }

        /// <summary>
        /// Reason why test failed.
        /// </summary>
        public string ErrorMessage { get; }

        /// <summary>
        /// Is test ran in the current test run cycle.
        /// </summary>
        public bool? NewResult { get; }

        /// <summary>
        /// Type of the directory.
        /// </summary>
        public DirectoryItemType Type { get; }

        /// <summary>
        /// Test run duration. 
        /// </summary>
        public TimeSpan ElapsedTime { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="testPassed"></param>
        /// <param name="errorMessage"></param>
        /// <param name="newResult"></param>
        /// <param name="type"></param>
        /// <param name="elapsedTime"></param>
        public TestResults(string filePath, bool? testPassed, string errorMessage, bool? newResult, DirectoryItemType type, TimeSpan elapsedTime)
        {
            FilePath = filePath;
            TestPassed = testPassed;
            ErrorMessage = errorMessage;
            NewResult = newResult;
            Type = type;
            ElapsedTime = elapsedTime;
        }
    }
}
