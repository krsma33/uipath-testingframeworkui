﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using WPFUI.Views;

namespace WPFUI.ViewModels
{
    /// <summary>
    /// The view model for the applications main Directory view
    /// </summary>
    public class DirectoryStructureViewModel : BaseViewModel
    {

        #region Static Events

        /// <summary>
        /// Event that is fired when clicking run all/selected tests. Event args hold .xaml file paths.
        /// </summary>
        public static event EventHandler<EventArgs<ICollection<string>>> GetTestsEvent;

        /// <summary>
        /// Event that is fired when clicking run all/selected tests. Event args hold information about previously ran tests.
        /// </summary>
        public static event EventHandler<EventArgs<Dictionary<string, bool>>> GetOldTestDataEvent;

        /// <summary>
        /// Event that is fired when clicking run all/selected tests. Event args hold information about window position, size and maximized status.
        /// </summary>
        public static event EventHandler<EventArgs<Dictionary<string, object>>> GetWindowInformationEvent;

        #endregion

        #region Public Properties

        /// <summary>
        /// A list of all directories
        /// </summary>
        public ObservableCollection<DirectoryItemViewModel> Items { get; set; }

        /// <summary>
        /// A list of currently selected items
        /// </summary>
        public ObservableCollection<DirectoryItemViewModel> SelectedItems { get; set; }

        /// <summary>
        /// Holds information to be displayed when tests are clicked
        /// </summary>
        public ResultDisplay DisplayedResults { get; set; }

        /// <summary>
        /// Dictionary that represents list of expanded tree items
        /// </summary>
        public static Dictionary<string, bool> ExpandedItems { get; set; } = new Dictionary<string, bool>();

        /// <summary>
        /// DirectoryItemViewModel of root folder
        /// </summary>
        public DirectoryItemViewModel RootItem { get; set; }

        /// <summary>
        /// Run All textbox foreground colour
        /// </summary>
        public Brush RunAllTextColour { get; set; }

        /// <summary>
        /// Controls which decoration will be used on Run All textbox
        /// </summary>
        public TextDecorationCollection RunAllMouseoverDecorations { get; set; }

        /// <summary>
        /// Run Selected textbox foreground colour
        /// </summary>
        public Brush RunSelectedTextColour { get; set; } = (SolidColorBrush)(new BrushConverter().ConvertFrom("#5c6076"));

        /// <summary>
        /// Controls which decoration will be used on Run Selected textbox
        /// </summary>
        public TextDecorationCollection RunSelectedMouseoverDecorations { get; set; }

        /// <summary>
        /// Action to close window
        /// </summary>
        public Action CloseAction { get; private set; }

        /// <summary>
        /// Project name along with the number of total tests found
        /// </summary>
        public string FullProjectName
        {
            get
            {
                string count = DirectoryStructure.GetXamlFilesCount(RootItem?.FullPath);

                if (TryParseTestCount(count) > 0)
                {
                    RunAllTextColour = (SolidColorBrush)(new BrushConverter().ConvertFrom("#01a539"));
                    RunAllMouseoverDecorations = TextDecorations.Underline;
                }
                else
                {

                    RunAllTextColour = (SolidColorBrush)(new BrushConverter().ConvertFrom("#5c6076"));
                    RunAllMouseoverDecorations = null;
                }

                return $"{ DirectoryStructureView.ProjectName } ({ count })";
            }
        }

        /// <summary>
        /// Stores all file paths that contain .xaml files for further use
        /// </summary>
        public static string[] EligibleFilePaths { get; private set; }

        /// <summary>
        /// Collection of Context Menu items for the running different predefined test templates (e.g. only failed tests, or only not run).
        /// </summary>
        public ObservableCollection<MenuItemViewModel> RunTestsTemplateActions { get; set; } = new ObservableCollection<MenuItemViewModel>();

        #endregion

        #region Public Commands

        /// <summary>
        /// Command to run when Run All is clicked
        /// </summary>
        public ICommand RunAllCommand { get; set; }

        /// <summary>
        /// Command to run when Run Selected is clicked
        /// </summary>
        public ICommand RunSelectedCommand { get; set; }

        /// <summary>
        /// Command to close the window
        /// </summary>
        public ICommand CloseCommand { get; set; }

        /// <summary>
        /// Command to go to open a specified file or folder location
        /// </summary>
        public ICommand OpenFileLocationCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public DirectoryStructureViewModel(Action closeAction)
        {
            CloseAction = closeAction;

            // Create commands
            RunAllCommand = new RelayCommand(RunAll, () => TryParseTestCount(RootItem?.TestCount) > 0);
            RunSelectedCommand = new RelayCommand(RunSelected, () => SelectedItems.Count > 0);
            CloseCommand = new RelayCommand(CloseWindow);
            OpenFileLocationCommand = new RelayCommand(OpenFileOrFolder);
            RunTestsTemplateActions.Add(new MenuItemViewModel("Run All Unrun", RunNotRan, () => FilterNotRanTests().Count > 0));
            RunTestsTemplateActions.Add(new MenuItemViewModel("Run All Failed", RunNotPassed, () => FilterNotPassedTests().Count > 0));
            RunTestsTemplateActions.Add(new MenuItemViewModel("Run All Passed", RunPassed, () => FilterPassedTests().Count > 0));

            LoadData();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Listens to the SelectedItems collection changed event. When event fires it checks if selected items count is greater than 0, and if so modifies text colour and decorations.
        /// </summary>
        private void SelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (SelectedItems.Count > 0)
            {
                RunSelectedTextColour = (SolidColorBrush)(new BrushConverter().ConvertFrom("#01a539"));
                RunSelectedMouseoverDecorations = TextDecorations.Underline;
            }
            else
            {
                RunSelectedTextColour = (SolidColorBrush)(new BrushConverter().ConvertFrom("#5c6076"));
                RunSelectedMouseoverDecorations = null;
            }

            DisplayTestResults();
        }

        #endregion

        #region Command Executions

        /// <summary>
        /// Gets path of the root folder and closes the window afterwards.
        /// </summary>
        private void RunAll()
        {
            CloseAction();
            GetTestsEvent.Raise(this, EligibleFilePaths);
            GetOldTestDataEvent.Raise(this, ExpandedItems);
            GetWindowInformationEvent.Raise(this, DirectoryStructureView.WindowInformation);
        }

        /// <summary>
        /// Gets path of the selected test(s) and closes the window afterwards.
        /// </summary>
        private void RunSelected()
        {
            CloseAction();
            GetTestsEvent.Raise(this, ParseSelectedItems());
            GetOldTestDataEvent.Raise(this, ExpandedItems);
            GetWindowInformationEvent.Raise(this, DirectoryStructureView.WindowInformation);
        }

        /// <summary>
        /// Gets paths of the not ran tests
        /// </summary>
        private void RunNotRan()
        {
            CloseAction();
            GetTestsEvent.Raise(this, FilterNotRanTests());
            GetOldTestDataEvent.Raise(this, ExpandedItems);
            GetWindowInformationEvent.Raise(this, DirectoryStructureView.WindowInformation);
        }

        /// <summary>
        /// Gets paths of the not passed tests
        /// </summary>
        private void RunNotPassed()
        {
            CloseAction();
            GetTestsEvent.Raise(this, FilterNotPassedTests());
            GetOldTestDataEvent.Raise(this, ExpandedItems);
            GetWindowInformationEvent.Raise(this, DirectoryStructureView.WindowInformation);
        }

        /// <summary>
        /// Gets paths of the passed tests
        /// </summary>
        private void RunPassed()
        {
            CloseAction();
            GetTestsEvent.Raise(this, FilterPassedTests());
            GetOldTestDataEvent.Raise(this, ExpandedItems);
            GetWindowInformationEvent.Raise(this, DirectoryStructureView.WindowInformation);
        }

        /// <summary>
        /// Closes the window of the WPF app
        /// </summary>
        private void CloseWindow()
        {
            CloseAction();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets paths of the passed tests
        /// </summary>
        private ICollection<string> FilterPassedTests()
        {
            var testResultsDictionary = DirectoryStructureView.TestResults;

            if (testResultsDictionary != null)
            {
                return testResultsDictionary.Where(x =>
                {
                    x.Value.TryGetValue("TestPassed", out object testStatus);
                    bool isFile = File.Exists(x.Key);
                    return ((bool)testStatus == true && isFile);
                }).Select(x => x.Key).ToArray();
            }
            else
            {
                return new List<string>();
            }

        }

        /// <summary>
        /// Gets paths of the not passed tests
        /// </summary>
        private ICollection<string> FilterNotPassedTests()
        {
            var testResultsDictionary = DirectoryStructureView.TestResults;

            if (testResultsDictionary != null)
            {
                return testResultsDictionary.Where(x =>
                {
                    x.Value.TryGetValue("TestPassed", out object testStatus);
                    bool isFile = File.Exists(x.Key);
                    return ((bool)testStatus != true && isFile);
                }).Select(x => x.Key).ToArray();
            }
            else
            {
                return new List<string>();
            }

        }

        /// <summary>
        /// Gets paths of the not ran tests
        /// </summary>
        private ICollection<string> FilterNotRanTests()
        {
            var testResultsDictionary = DirectoryStructureView.TestResults;

            if (testResultsDictionary != null)
            {
                var itemsToRemove = testResultsDictionary.Select(x => x.Key);
                return EligibleFilePaths.Where(x => !itemsToRemove.Contains(x)).ToList();
            }

            return EligibleFilePaths;

        }

        /// <summary>
        /// Sets the DisplayedResults property based on selected items
        /// </summary>
        private void DisplayTestResults()
        {
            if (SelectedItems.Count == 0)
            {
                DisplayedResults = null;
            }
            else if (SelectedItems.Count == 1)
            {
                var selected = SelectedItems[0];
                bool? testResult = selected.TestResults.TestPassed;
                string errorMessage = selected.TestResults.ErrorMessage;
                string elapsedTime = selected.TestResults.ElapsedTime.ToString();

                if (selected.Type == DirectoryItemType.File && selected.TestResults.TestPassed != null)
                {
                    DisplayedResults = new ResultDisplay(Path.GetFileNameWithoutExtension(selected.FullPath), selected.FullPath, testResult, errorMessage, elapsedTime, null, null, null);
                }
                else if (selected.Type == DirectoryItemType.File && selected.TestResults.TestPassed == null)
                {
                    DisplayedResults = new ResultDisplay(Path.GetFileNameWithoutExtension(selected.FullPath), selected.FullPath, null, null, null, null, null, null);
                }
                else if (selected.Type != DirectoryItemType.File && (selected.TestResults.NewResult == false || selected.TestResults.NewResult == null))
                {
                    DisplayedResults = new ResultDisplay(DirectoryStructure.GetFileFolderName(selected.FullPath), selected.FullPath, null, null, null, null, null, null);
                }
                else if (selected.Type != DirectoryItemType.File && selected.TestResults.TestPassed != null && selected.TestResults.NewResult == true)
                {
                    GetTestResultsSummaryAndSetDisplayedResults(selected.FullPath, DirectoryStructure.GetFileFolderName(selected.FullPath));
                }
                else
                {
                    GetTestResultsSummaryAndSetDisplayedResults(DirectoryStructureView.RootFolderPath, "Complete Test Run");
                }
            }
            else
            {
                GetTestResultsSummaryAndSetDisplayedResults(DirectoryStructureView.RootFolderPath, "Complete Test Run");
            } 

        }

        /// <summary>
        /// Get all test results and find summary
        /// </summary>
        private void GetTestResultsSummaryAndSetDisplayedResults(string folderPath, string title)
        {
            if (DirectoryStructureView.TestResults != null)
            {
                var filteredItems = DirectoryStructureView.TestResults?.Where(x => x.Key.Replace("/", "\\").Contains(folderPath.Replace("/", "\\") + "\\")).Where(x =>
                {
                    x.Value.TryGetValue("NewResult", out object newResult);
                    bool isFile = File.Exists(x.Key);
                    return ((bool)newResult == true && isFile);
                }).Select(x => x.Value).ToArray();

                if (filteredItems?.Count() > 0)
                {
                    var totalElapsedTime = filteredItems.Select(x => { x.TryGetValue("ElapsedTime", out object elapsedTime); return (TimeSpan)elapsedTime; }).Aggregate((x, y) => x.Add(y));

                    var passedTests = filteredItems.Where(x => { x.TryGetValue("TestPassed", out object testPassed); return (bool?)testPassed == true; });
                    var passedCount = passedTests.Count();
                    string totalPassed = passedCount > 0 ? String.Format("Passed Tests: {0}", passedCount) : null;

                    var failedTests = filteredItems.Where(x => { x.TryGetValue("TestPassed", out object testPassed); return (bool?)testPassed == false; });
                    var failedCount = failedTests.Count();
                    string totalFailed = failedCount > 0 ? String.Format("Failed Tests: {0}", failedCount) : null;

                    string header = title == "Complete Test Run" ? "Complete Test Run Summary" : String.Format("Test Run Summary - {0}", title);
                    string folder = title == "Complete Test Run" && SelectedItems.Count == 1 ? SelectedItems[0].FullPath : title == "Complete Test Run" ? null : folderPath;


                    DisplayedResults = new ResultDisplay(header, folder, null, null, null, totalElapsedTime.ToString(), totalPassed, totalFailed);
                }
            }
            else
            {
                DisplayedResults = null;
            }
        }

        /// <summary>
        /// Gets root folder and its children.
        /// </summary>
        /// <returns></returns>
        private void LoadData()
        {
            EligibleFilePaths = DirectoryStructure.GetXamlFilesPaths(DirectoryStructureView.RootFolderPath).ToArray();
            ExpandedItems = DirectoryStructureView.ExpandedItems ?? new Dictionary<string, bool>();
            RootItem = new DirectoryItemViewModel(DirectoryStructureView.RootFolderPath, DirectoryItemType.Folder, RunSelected);
            SelectedItems = new ObservableCollection<DirectoryItemViewModel>();
            SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;

            var children = new List<DirectoryItemViewModel> { RootItem };
            Items = new ObservableCollection<DirectoryItemViewModel>(children);
        }

        /// <summary>
        /// Parses DirectoryItemViewModel TestCount string as number of tests found.
        /// </summary>
        /// <param name="testCount"></param>
        /// <returns></returns>
        private int TryParseTestCount(string testCount)
        {
            if (testCount == null)
                return 0;

            var parsedString = testCount.Replace("(", "").Replace(")", "");

            if (!Int32.TryParse(parsedString, out int result))
            {
                return 0;
            }

            return result;
        }

        
        /// <summary>
        /// Parses the selected items to retrieve list of runnable .xaml files
        /// </summary>
        /// <returns></returns>
        private HashSet<string> ParseSelectedItems()
        {
            var paths = new HashSet<string>();

            foreach (var item in SelectedItems)
            {
                if (item.Type == DirectoryItemType.File)
                {
                    paths.Add(item?.FullPath);
                }
                else
                {
                    paths.UnionWith(DirectoryStructure.GetXamlFilesPaths(item.FullPath));
                }
            }

            return paths;
        }

        /// <summary>
        /// Opens a file or folder location
        /// </summary>
        private void OpenFileOrFolder()
        {
            if (File.Exists(DisplayedResults.FullPath))
            {
                var process = new ProcessStartInfo("explorer.exe", " /select, " + DisplayedResults.FullPath);
                Process.Start(process);
            }
            else if (Directory.Exists(DisplayedResults.FullPath))
            {
                var process = new ProcessStartInfo("explorer.exe", DisplayedResults.FullPath);
                Process.Start(process);
            }
        }

        #endregion

    }
}
