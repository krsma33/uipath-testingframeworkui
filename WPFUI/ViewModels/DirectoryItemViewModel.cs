﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WPFUI.ViewModels
{
    /// <summary>
    /// A view model for each directory item
    /// </summary>
    public class DirectoryItemViewModel : BaseViewModel
    {

        #region Public Properties

        /// <summary>
        /// Results of Testing
        /// </summary>
        public TestResults TestResults { get; set; }

        /// <summary>
        /// The type of this item
        /// </summary>
        public DirectoryItemType Type { get; set; }

        /// <summary>
        /// The full path to the item
        /// </summary>
        public string FullPath { get; set; }

        /// <summary>
        /// The name of this directory item
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Number of tests found in the subfolders.
        /// </summary>
        public string TestCount { get; private set; }

        /// <summary>
        /// A list of all children contained inside this item
        /// </summary>
        public ObservableCollection<DirectoryItemViewModel> Children { get; set; }

        /// <summary>
        /// Idicates if this item can be expanded
        /// </summary>
        public bool CanExpand { get { return Type != DirectoryItemType.File; } }

        /// <summary>
        /// Indicates if the current item is expanded or not
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return Children?.Count(x => x != null) > 0;
            }

            set
            {
                // If the UI tells us to expand...
                if (value == true)
                {
                    // Find all children
                    ExpandAsync().FireAndForgetSafeAsync();
                }
                // If the UI tells us to close
                else
                {
                    CollapseAll();
                }
            }
        }

        /// <summary>
        /// Indicates if this item is selected
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Collection of Context Menu items when tree item is rightclicked.
        /// </summary>
        public ObservableCollection<MenuItemViewModel> RightClickActions { get; set; } = new ObservableCollection<MenuItemViewModel>();

        /// <summary>
        /// Performs action from the main view model. Runs the selected items.
        /// </summary>
        public Action RunSelectedParentAction { get; set; }

        #endregion

        #region Public Commands

        /// <summary>
        /// The command to expand this item
        /// </summary>
        public IAsyncCommand ExpandCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="fullPath">The full path of this item</param>
        /// <param name="type">The type of the item</param>
        public DirectoryItemViewModel(string fullPath, DirectoryItemType type, Action runSelectedFromParent)
        {
            // Set properties
            FullPath = fullPath;
            TestResults = DirectoryStructure.SetTestResults(fullPath, type);
            Type = type;

            // Create commands
            // ExpandCommand = new RelayCommandAsync(ExpandAsync);
            RightClickActions.Add(new MenuItemViewModel("Run Selected", RunSelected));
            RightClickActions.Add(new MenuItemViewModel("Expand All", ExpandAll, () => Type != DirectoryItemType.File));
            RightClickActions.Add(new MenuItemViewModel("Collapse All", CollapseAll, () => Type != DirectoryItemType.File && IsExpanded));
            RightClickActions.Add(new MenuItemViewModel(Type == DirectoryItemType.File ? "Edit Test" : "Open Test Folder", OpenFileOrFolder));
            RunSelectedParentAction = runSelectedFromParent;

            // Setup
            ClearChildren();
            GetItemNameAsync().FireAndForgetSafeAsync();
            GetTestItemsCountAsync().FireAndForgetSafeAsync();

            // Set is expanded
            IsExpanded = WasExpandedBefore();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Checks if specified path was expanded in erlier runs. Purpose is simulatin persistence during multiple app runs.
        /// </summary>
        /// <returns></returns>
        private bool WasExpandedBefore()
        {
            if (DirectoryStructureViewModel.ExpandedItems == null)
                return false;

            DirectoryStructureViewModel.ExpandedItems.TryGetValue(FullPath, out bool result);

            return result;
        }

        /// <summary>
        /// Checks if specified path was expanded in erlier runs. Purpose is simulatin persistence during multiple app runs.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> WasExpandedBeforeAsync()
        {
            return await Task.Run(() => WasExpandedBefore());
        }

        /// <summary>
        /// Gets Item Name async
        /// </summary>
        /// <returns></returns>
        private async Task GetItemNameAsync()
        {
            if (Type == DirectoryItemType.Drive)
            {
                Name = FullPath;
            }
            else
            {
                Name = await Task.Run(() => DirectoryStructure.GetFileFolderName(FullPath));
            }
        }

        /// <summary>
        /// Gets count of the .xaml files in a folder and its sub-folders
        /// </summary>
        /// <returns></returns>
        private async Task GetTestItemsCountAsync()
        {
            if (Type == DirectoryItemType.File)
            {
                TestCount = String.Empty;
            }
            else
            {
                var count = await Task.Run(() => GetXamlFilesCount());
                TestCount = $" ({ count })";
            }
        }

        /// <summary>
        /// Gets total number of .xaml files that are in all of the subfolders of this object Path
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        private string GetXamlFilesCount()
        {
            string result;

            switch (Type)
            {
                case DirectoryItemType.File:
                    result = String.Empty;
                    break;
                default:
                    result = Directory.EnumerateFiles(FullPath, "*.xaml", SearchOption.AllDirectories).Count().ToString();
                    break;
            }

            return result;
        }

        #endregion

        #region Command Executions

        /// <summary>
        /// Expands this directory and finds all children
        /// </summary>
        private void Expand()
        {
            // We cannot expand a file
            if (Type == DirectoryItemType.File)
                return;

            // Find all children
            if (Children.Count(x => x != null) > 0 == false)
            {
                var children = DirectoryStructure.GetDirectoryContents(FullPath, DirectoryStructureViewModel.EligibleFilePaths);
                Children = new ObservableCollection<DirectoryItemViewModel>(
                                children.Select(content => new DirectoryItemViewModel(content.FullPath, content.Type, RunSelectedParentAction)));
            }

            DirectoryStructureViewModel.ExpandedItems[FullPath] = true;
        }

        /// <summary>
        /// Expands this directory and finds all children async
        /// </summary>
        /// <returns></returns>
        private async Task ExpandAsync()
        {
            // We cannot expand a file
            if (Type == DirectoryItemType.File)
                return;

            // Find all children
            if (Children.Count(x => x != null) > 0 == false)
            {
                var children = await DirectoryStructure.GetDirectoryContentsAsync(FullPath, DirectoryStructureViewModel.EligibleFilePaths);
                Children = await Task.Run(async () => await Task.Run(() => new ObservableCollection<DirectoryItemViewModel>(
                    children.Select(content => new DirectoryItemViewModel(content.FullPath, content.Type, RunSelectedParentAction)))));
            }

            DirectoryStructureViewModel.ExpandedItems[FullPath] = true;
        }

        /// <summary>
        /// Expands this directory and all its children
        /// </summary>
        private void ExpandAll()
        {
            ExpandAllAsync().FireAndForgetSafeAsync();
        }

        /// <summary>
        /// Expands this directory and all its children async
        /// </summary>
        private async Task ExpandAllAsync()
        {
            await ExpandAsync();

            await Children.ForEachAsync(async x => await x.ExpandAllAsync());
        }

        /// <summary>
        /// Collapses all children tree view items async.
        /// </summary>
        private void CollapseAll()
        {
            ClearChildren();

            DirectoryStructureViewModel.ExpandedItems[FullPath] = false;
        }

        /// <summary>
        /// Invokes parent action to run selected items.
        /// </summary>
        private void RunSelected()
        {
            RunSelectedParentAction?.Invoke();
        }

        /// <summary>
        /// Clear selected status in children recuresively
        /// </summary>
        /// <param name="children">ObservableCollection<DirectoryItemViewModel> object to clear selected status from</param>
        private void ClearSelectedInChildren(ObservableCollection<DirectoryItemViewModel> children)
        {
            ClearSelectedInChildrenAsync(children).FireAndForgetSafeAsync();
        }

        /// <summary>
        /// Clear selected status in children recuresively asynchrounously
        /// </summary>
        /// <param name="children">ObservableCollection<DirectoryItemViewModel> object to clear selected status from</param>
        private async Task ClearSelectedInChildrenAsync(ObservableCollection<DirectoryItemViewModel> children)
        {
            // Clear selected status recursively
            if (children != null && children.Count > 0)
            {
                foreach (var child in children)
                {
                    if (child != null)
                    {
                        child.IsSelected = false;
                        await child.ClearSelectedInChildrenAsync(child.Children);
                        DirectoryStructureViewModel.ExpandedItems[child.FullPath] = false;
                    }
                }
            }
        }

        /// <summary>
        /// Removes all children from the list, adding a dummy item to the expand icon if required
        /// </summary>
        private void ClearChildren()
        {
            ClearSelectedInChildren(Children);

            Children = new ObservableCollection<DirectoryItemViewModel>();

            // Show the expand arrow if we are not a file
            if (Type != DirectoryItemType.File)
                Children?.Add(null);
        }

        /// <summary>
        /// Opens a file or folder location
        /// </summary>
        private void OpenFileOrFolder()
        {
            var process = new ProcessStartInfo("explorer.exe", FullPath);
            Process.Start(process);
        }

        #endregion

    }
}
