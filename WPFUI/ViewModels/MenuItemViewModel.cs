﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPFUI.ViewModels
{
    public class MenuItemViewModel : BaseViewModel
    {
        public string Header { get; private set; }
        public ICommand Command { get; private set; }

        public MenuItemViewModel(string header, Action execute, Func<bool> canExecute = null)
        {
            Header = header;
            Command = new RelayCommand(execute, canExecute);
        }
    }
}
