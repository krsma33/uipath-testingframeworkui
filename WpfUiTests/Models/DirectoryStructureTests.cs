﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WPFUI.Tests
{
    [TestClass()]
    public class DirectoryStructureTests
    {
        [TestMethod()]
        public void GetXamlFilesPathsTest()
        {
            string filePath = @"D:\Documents for Projects";
            var a = DirectoryStructure.GetXamlFilesPaths(filePath);

            foreach (var item in a)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("\r\n");

            var b = Directory.GetDirectories(filePath);


            //foreach (var item in b)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine("\r\n");

            //var c = b.Where(x => a.Contains(x));

            //foreach (var item in c)
            //{
            //    Console.WriteLine(item);
            //}

            if (a.Any(y => y.Contains(@"D:\Documents for Projects\03_Uipath\DummyProject-LevenshteinDistance")))
            {
                Console.WriteLine(true);
            }
            else
            {
                Console.WriteLine(false);
            }

        }

        [TestMethod()]
        public void Test()
        {
            string timespanStr = "00:00:00.207";
            string timespanStr2 = "00:00:01.313";

            var timespan = TimeSpan.Parse(timespanStr);
            var timespan2 = TimeSpan.Parse(timespanStr2);
            var timespan3 = timespan.Add(timespan2);
            Console.WriteLine(timespan3);

            var timespans = new List<TimeSpan> { timespan, timespan2, timespan3 };

            var result = timespans.Aggregate((x, y) => x.Add(y));

            Console.WriteLine(result);

        }

        [TestMethod()]
        public void Test2()
        {
            Assert.Fail();

        }
    }
}