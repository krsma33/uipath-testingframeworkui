﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using WPFUI.Views;
using WPFUI;
using WPFUI.ViewModels;

namespace WpfUi.Tests
{
    [TestClass()]
    public class MainWindowTests
    {
        [TestMethod()]
        public void MainWindowTest()
        {
            DirectoryStructureView window = null;

            // The dispatcher thread
            var t = new Thread(() =>
            {
                DirectoryStructureView.RootFolderPath = @"D:\Documents for Projects";
                DirectoryStructureView.ProjectName = "Uber Project Yo";
                var dictionary = new SortedDictionary<string, Dictionary<string, object>>()
                {
                    {@"D:\Documents for Projects\01_C#\CustomActivities.Email\CustomActivities.Email.Design\DeleteMailFolderDesigner.xaml",
                        new Dictionary<string, object>()
                        {
                            { "TestPassed", true }, { "ErrorMessage", null}, { "NewResult", false }, {"ElapsedTime", new TimeSpan(0,0,0,0,100) }
                        }
                    },
                    {@"D:\Documents for Projects\01_C#\CustomActivities.VariableComparer\CustomActivities.VariableComparer.Design\CustomActivityDesigner.xaml",
                        new Dictionary<string, object>()
                        {
                            { "TestPassed", false }, { "ErrorMessage", "Test execution stopped due to an error.\nType: UiPath.Core.BusinessRuleException.\nMessage: hoho Robo.\nSource: Throw."}, { "NewResult", true }, {"ElapsedTime", new TimeSpan(0,0,0,2,993) }
                        }
                    },
                    {@"D:\Documents for Projects\01_C#\Learning\DocusignUiPath-master\BenMann.Docusign.Activities.Design\Build\Envelopes\SendEnvelopeActivityDesigner.xaml",
                        new Dictionary<string, object>()
                        {
                            { "TestPassed", true }, { "ErrorMessage", null}, { "NewResult", true }, {"ElapsedTime", new TimeSpan(0,0,0,0,610) }
                        }
                    },
                    {@"D:\Documents for Projects\01_C#\Learning\DocusignUiPath-master\BenMann.Docusign.Activities.Design\Build\Tabs\Display\AddDateSignedActivityDesigner.xaml",
                        new Dictionary<string, object>()
                        {
                            { "TestPassed", false }, { "ErrorMessage", "Random error occured!"}, { "NewResult", false }, {"ElapsedTime", new TimeSpan(0,0,0,1,234) }
                        }
                    }
                };
                var IsExpanded = new Dictionary<string, bool>()
                {
                    { @"D:\Documents for Projects", true },
                    { @"D:\Documents for Projects\01_C#", true },
                    { @"D:\Documents for Projects\01_C#\CustomActivities.VariableComparer", true },
                };

                DirectoryStructureView.TestResults = dictionary;
                DirectoryStructureView.ExpandedItems = IsExpanded;
                //DirectoryStructureView.WindowInformation = new Dictionary<string, object>()
                //{
                //    { "IsMaximized", true },
                //    { "Top", 85d },
                //    { "Left", 3000d },
                //    { "Height", 750d },
                //    { "Width", 1200d }
                //};

                window = new DirectoryStructureView();

                DirectoryStructureViewModel.GetTestsEvent += DirectoryStructureViewModel_RunTestsEvent;
                DirectoryStructureViewModel.GetOldTestDataEvent += DirectoryStructureViewModel_RunTestsEvent2;

                // Initiates the dispatcher thread shutdown when the window closes
                window.Closed += (s, e) => window.Dispatcher.InvokeShutdown();

                window.Show();

                // Makes the thread support message pumping
                System.Windows.Threading.Dispatcher.Run();
            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }

        private void DirectoryStructureViewModel_RunTestsEvent2(object sender, EventArgs<Dictionary<string, bool>> e)
        {
            foreach (var item in e.Value)
            {
                Console.WriteLine("IsExpanded: " + item);
            }
        }

        private void DirectoryStructureViewModel_RunTestsEvent(object sender, EventArgs<ICollection<string>> e)
        {
            foreach (var item in e.Value)
            {
                Console.WriteLine("Files to run: " + item);
            }
        }
    }
}