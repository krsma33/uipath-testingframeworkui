﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using WPFUI;
using WPFUI.ViewModels;
using WPFUI.Views;

namespace TestingFrameworkUi
{
    [Description("User Interface component used to select .xaml files from the tree view and retrieve full file paths based on the selection.")]
    public class GetTestLocationsUi : CodeActivity
    {
        [RequiredArgument, Category("Input")]
        [Description("Input path of the initial folder to search tests from")]
        public InArgument<string> RootTestFolder { get; set; }

        [Category("Input")]
        [Description("Name of the project")]
        public InArgument<string> ProjectName { get; set; }

        [RequiredArgument, Category("Output")]
        [Description("Dictionary that contains list of tests to be run. Keys represent the test folder names. Values represent list of test paths that belong to specified folder.")]
        public OutArgument<Dictionary<string, List<string>>> TestsToRun { get; set; }

        [RequiredArgument, Category("Input/Output")]
        [Description("Information about already runned tests. This is used so that information is persisted for the re-runs. On the first test run it should be null. If not empty following keys will be present in the dictionary: TestPassed of type bool, ErrorMessage of type string, NewResult of type bool, and ElapsedTime of type string")]
        public InOutArgument<SortedDictionary<string, Dictionary<string, object>>> TestRunInformation { get; set; }

        [RequiredArgument, Category("Input/Output")]
        [Description("Information about expanded items in previous test run. This is used so that information is persisted for the re-runs. On the first test run it should be null.")]
        public InOutArgument<Dictionary<string, bool>> ExpandedItems { get; set; }

        [RequiredArgument, Category("Input/Output")]
        [Description("Information about UI position, size and maximized state. This is used so that information is persisted for the re-runs. On the first test run it should be null.")]
        public InOutArgument<Dictionary<string, object>> WindowInformation { get; set; }

        private ICollection<string> _tests;
        private Dictionary<string, bool> _expandedItems;
        private Dictionary<string, object> _windowInformation;

        protected override void Execute(CodeActivityContext context)
        {
            string folder = GetRootFolderPath(RootTestFolder.Get(context));
            var testRunInfo = TestRunInformation.Get(context);
            var expandedItems = ExpandedItems.Get(context);
            var windowInformation = WindowInformation.Get(context);

            if (!Directory.Exists(folder))
                throw new ArgumentException("Argument must point to a valid folder", "RootTestFolder");

            OpenSelectTestsWindow(folder, ProjectName.Get(context), testRunInfo, expandedItems, windowInformation);

            TestsToRun.Set(context, RearrangeTests(_tests, folder));
            TestRunInformation.Set(context, SetNewResultToFalse(testRunInfo));
            ExpandedItems.Set(context, _expandedItems);
            WindowInformation.Set(context, _windowInformation);
        }

        private SortedDictionary<string, Dictionary<string, object>> SetNewResultToFalse(SortedDictionary<string, Dictionary<string, object>> dictionary)
        {
            var resultDictionary = new SortedDictionary<string, Dictionary<string, object>>();

            if (dictionary == null)
                return resultDictionary;

            foreach (var kvp in dictionary)
            {
                var objectDictionary = new Dictionary<string, object>();

                foreach (var kvp2 in kvp.Value)
                {
                    if (kvp2.Key == "NewResult")
                    {
                        objectDictionary[kvp2.Key] = false;
                    }
                    else
                    {
                        objectDictionary[kvp2.Key] = kvp2.Value;
                    }
                }

                resultDictionary[kvp.Key] = objectDictionary;
            }

            return resultDictionary;
        }

        private Dictionary<string, List<string>> RearrangeTests(ICollection<string> tests, string folderPath)
        {
            var resultDictionary = new Dictionary<string, List<string>>();

            if (tests == null)
                return resultDictionary;

            string basePath = Path.GetDirectoryName(folderPath);

            foreach (var test in tests)
            {
                var folder = test.Substring(basePath.Length + 1);
                folder = folder?.Substring(0, (folder.Length - Path.GetFileName(test).Length - 1));

                if (resultDictionary.TryGetValue(folder, out List<string> testList))
                {
                    testList?.Add(test);
                    resultDictionary[folder] = testList;
                }
                else
                {
                    testList = new List<string> { test };
                    resultDictionary[folder] = testList;
                }
            }

            return resultDictionary;
        }

        private string GetRootFolderPath(string rootTestFolderPath)
        {
            string folder = String.IsNullOrWhiteSpace(rootTestFolderPath) ?
                    Directory.GetCurrentDirectory() : Path.IsPathRooted(rootTestFolderPath) ?
                        rootTestFolderPath : Path.Combine(Directory.GetCurrentDirectory(), rootTestFolderPath);

            return folder;
        }

        private void OpenSelectTestsWindow(string rootFolderPath, string projectName, SortedDictionary<string, Dictionary<string, object>> testRunInformation, Dictionary<string, bool> expandedItems, Dictionary<string, object> windowInformation)
        {
            DirectoryStructureView window = null;

            // The dispatcher thread
            var t = new Thread(() =>
            {
                DirectoryStructureView.RootFolderPath = rootFolderPath;
                DirectoryStructureView.ProjectName = projectName.Replace(Environment.NewLine, " ") ?? "UiPath Testing Framework";
                DirectoryStructureView.TestResults = testRunInformation;
                DirectoryStructureView.ExpandedItems = expandedItems;
                DirectoryStructureView.WindowInformation = windowInformation;

                window = new DirectoryStructureView();

                DirectoryStructureViewModel.GetTestsEvent += DirectoryStructureViewModel_RunTestsEvent;
                DirectoryStructureViewModel.GetOldTestDataEvent += DirectoryStructureViewModel_RunTestsEvent2;
                DirectoryStructureViewModel.GetWindowInformationEvent += DirectoryStructureViewModel_GetWindowInformationEvent;

                // Initiates the dispatcher thread shutdown when the window closes
                window.Closed += (s, e) => window.Dispatcher.InvokeShutdown();

                window.Show();

                // Make window show on top
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();

                // Makes the thread support message pumping
                System.Windows.Threading.Dispatcher.Run();
            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }

        private void DirectoryStructureViewModel_GetWindowInformationEvent(object sender, EventArgs<Dictionary<string, object>> e)
        {
            _windowInformation = e.Value;
        }

        private void DirectoryStructureViewModel_RunTestsEvent2(object sender, EventArgs<Dictionary<string, bool>> e)
        {
            _expandedItems = e.Value;
        }

        private void DirectoryStructureViewModel_RunTestsEvent(object sender, EventArgs<ICollection<string>> e)
        {
            _tests = e.Value;
        }
    }
}
